# BISM: Bytecode Instrumentation for Software Monitoring

```BISM``` is  a lightweight bytecode instrumentation tool for Java programs that is built on top of [ASM](https://asm.ow2.io/).
 It features an expressive high-level instrumentation language. The language is inspired by AOP but adopts an instrumentation model that is more directed towards runtime verification. In particular, BISM provides separate classes, transformers, that encapsulate joinpoint selection and advice inlining. It offers various instrumentation selectors that select joinpoints covering bytecode instructions, basic blocks, and methods execution. BISM also provides access to a set of comprehensive joinpoint-related static and dynamic contexts to retrieve relevant information. BISM provides a set of advice methods that specify the advice to insert code, invoke methods, and print information. 
 
BISM is control-flow aware. That is, it generates CFGs for all methods and provides access to them in its language. Moreover, BISM provides several control-flow properties, such as capturing conditional jump branches and retrieving successor and predecessor basic blocks. Such features can provide support to tools relying on a control-flow analysis, for instance, in the security domain, to check for control-flow integrity. BISM also provides a mechanism to compose multiple transformers and automatically detect their collision in the base program. Transformers in a composition are capable of controlling the visibility of their advice and of other original instructions. BISM is a standalone tool implemented in Java that requires no installation, as compared to AspectJ and DiSL. BISM can run in two instrumentation modes: build-time to statically instrument a Java class or Jar file, and load-time to run as a Java agent that instruments classes being loaded by a running JVM.

- [BISM: Bytecode Instrumentation for Software Monitoring](#bism--bytecode-instrumentation-for-software-monitoring)
- [Request BISM](#request-to-download-bism)
- [How to Run](#how-to-run)
    * [Run args](#run-args)
    * [Config file](#config-file)
    * [Run](#run)
- [BISM Language](#bism-language)
  * [Advice Methods](#advice-methods)
  * [Join Points](#join-points)
  * [Static Context](#static-context)
  * [Dynamic Context](#dynamic-context)
- [Experiments](#experiments)
- [Contact Us](#contact-us)

# Request to Download BISM

If you are interested in the tool, request to download BISM by filling the form.

[Request to Download BISM](https://framaforms.org/request-to-use-bism-1626425274).

# How to Run

BISM can be run in 2 modes: **Load-time** as a Java Agent (java.lang.instrument), or **Built-time Instrumentation**. 
When run in load-time mode, the raw bytes are passed to BISM and instrumented before the linking phase. When run build-time mode, the source code is modified
and written back to the files (always making a backup in the same path).

## Run args 

To run BISM, you need to specify the run arguments.

1. transformer(s) [required]
    * You can choose built-in transformers from the examples folder by specifying the name only. Example, transformer=TestInversion
    * or Custom Transformer
        * Check below for implementing new transformers
        * Specify the file path of the compiled transformer
        * Example, transformer=.../CustomTransformer.class
    * Multiple transformers can be applied (,) seperated   
2. target [required] (build-time only)
    * File path to a compiled Java class or a Jar file to be instrumented   
    * Example target=.../SimpleJavaProgram.jar
3. scope [optional]
    * Specify packages, classes, and methods to be instrumented.
    * Leave it empty to instrument all classes.
    * Example, scope=mypackage.\*,package.Main.\*,Test.main 
         * Instruments all classes and methods under mypackage, all methods under package.Main, and method main in Test class
4. blacklist [optional]
    * Same as scope
5. visualize [optional]
    * Tells BISM to print html files representing the control flow graphs.
    * For each instrumented class, the original and instrumented cfg for every instrumented method will be available to visualize. These can be checked at ```jar/html/graphs/class/method```
    * Does not take any value  
6. output [optional] (load-time only)
    * Path to directory for writing instrumented classes
7. config [optional]
    * Path to an xml file similar to the one in `examples/to-be-filled/bism.xml`


## Config file  

The config file allows the user to specify all the above arguments (except config) in one file instead of specifying them separately on each run.

The configuration file format is as the following example. The config file should contain all the run arguments needed.

```xml
<?xml version="1.0" encoding="utf-8" ?>

<bism>
    <config>
        <transformer value="AdviceTransformer"/>
        <target value="pathtojarorclass"/>
        <scope value="mypackage.*"/> <!-- optionnal -->
        <blacklist value="myotherpackage.*"/> <!-- optionnal -->
        <visualize/>  <!-- empty and optionnal -->
        <output value="/instrumented"/> <!--  optionnal -->
    </config>
    <!-- You have here to put as many <transformer>..</> as you have transformer composed, in the same order-->
    <transformer>
        <!-- As many arguments as needed by AdviceTransformer [optionnal] -->
        <arg key="field" value="mySuperField"/>
    </transformer>
</bism>
```

## Run

Run the jar from binaries folder (you need Java 8 and above):

### Build-time Instrumentation: 

With separate arguments:

``` 
java -jar bism.jar transformer=AdviceTransformer:target=../SimpleJavaProgram.jar:scope=SimpleProgram.main:visualize 
```

Alternatively, for config files

``` 
java -jar bism.jar config=build.xml 
```

### Load-time Instrumentation: 

For example, to run as Java Agent (the target jar/class is provided as an argument to the *java* command not BISM).

With separate arguments:

``` 
java -javaagent:bism.jar=transformer=AdviceTransformer:scope=SimpleProgram.main,SimpleProgram.foo:output=/instrumented -jar .../SimpleJavaProgram.jar 
```

Alternatively, for config files

``` 
java -javaagent:bism.jar=config=build.xml  -jar .../SimpleJavaProgram.jar 
```


## Custom Transformers

To implement a new Transformer, you need to extend **inria.bism.transformers.Transformer** from the bism.jar and implement the joinpoints you are insterested in.

You can look for examples and copy a transformer from the **examples** folder. All transformers in that directory are built-in with bism.jar

### Compile and Run 
After that you need to compile the Transformer with bism.jar in classpath
```
javac -cp ".:bism.jar:" MyTransformer.java 
```
and then run BISM with the transformer argument set to the path of your compiled class.

 
# BISM Language

 
Instrumentation is implemented in a Transformer class. You can run any of the existing transformers in the examples folder, as they are compiled into the jar. To implement a new Transformer, you need to create separately a new Java class that extends `inria.bism.transformers.Transformer` from the `bism.jar`.  The sections below describe the Transformer API. How instrumentation can be achieved, what join points are available, and what static and dynamic context is available at each join point.


##  Advice Methods  

A user inserts advice into the base program at the captured joinpoints using the advice instrumentation methods. Advice methods allow the user to extract needed static and dynamic information from within joinpoints, also allowing arbitrary bytecode insertion. These methods are invoked within selectors.  

We list below all the transformer api methods:



###  Printing on Console

These methods are used to instrument print commands.

```void println(String message)```

Used to print a message on the console and a new line after

```void print(String message)```

Used to print a message on the console

```void print(DynamicValue dv)```

Used to print the toString() of a dynamic value (local variable) on the console.

```void printHash(DynamicValue dv)```

Used to print the System.identityHashCode() of a dynamic value (local variable).
This is useful for getting unique ids for objects.


```void print(String message, boolean err)```

Same as *print(String message)*  but if passed boolean true, the print stream will be ```err```.

```void print(DynamicValue dv, boolean err)```

Same as *print(DynamicValue dv)* but if passed boolean true, the print stream will be ```err```.

```void printHash(DynamicValue dv, boolean err)```

Same as *printHash(DynamicValue dv)* but if passed boolean true, the print stream will be ```err```.

Below is an example that prints the method name and result after a method call:
```java
@Override
public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

    DynamicValue result = dc.getMethodResult(methodCall);

    print(mc.methodName + " result:");  //print message
    print(result);  //print a dynamic value
}
```


### Method Invocation


This method is used to instrument method invocation.

```void invoke(StaticInvocation smi)```

Used to invoke a static method. Methods can be external, note that to run the instrumented code, the method owner class should be included in the classpath. ```StaticInvocation``` class should be passed to ```invoke```. The datatype is simple. The constructor takes in class name and method name. ```addParameter()``` is then called to add parameters to the onvocation.
  
  Below is an example of invoking a static method (representing a monitor) and passing some dynamic ans static context:

```java
@Override
public void beforeMethodCall(MethodCall methodCall, MethodCallDynamicContext dc) {

    if (!methodCall.methodName.equals("callMe"))  return;

    DynamicValue p1 = dc.getMethodArgs(methodCall, 1); //get first argument of a method call
   
    // pass in full class name and method name
    StaticInvocation sti = new StaticInvocation("test/IteratorTest", "monitorCallMe");
    sti.addParameter(p1); // pass the dynamic value
    sti.addParameter(5);  //pass a primitve type

    invoke(sti);  //invoke
}
```

The ```addParameter``` only accepts either a ```DynamicValue``` or a primitive type. Any other type will be ignored.

### Bytecode Instructions Insertion


These methods are used to instrument bytecode instructions.

 ```insert(AbstractInsnNode ins)``` 

Used for manually inserting an ASM bytecode instructions.

```insert(List<AbstractInsnNode> ins)```

Used for manually inserting a list of ASM bytecode instructions.

Below is an example of inserting a DUP instruction in order to reevaluate
a conditional jump after the jump occurs:
```java
@Override
public void beforeInstruction(Instruction ins, InstructionDynamicContext dc) {
    if (ins.isConditionalJump) {
        if (ins.stackOperandsCountIfConditionalJump == 1)
            insert(new InsnNode(Opcodes.DUP)); //insert DUP
        else
            insert(new InsnNode(Opcodes.DUP2)); //insert DUP2
    }
}
  ```

  ----
  ## Selectors

Selectors provide a mechanism to select joinpoints and specify the advice. They are implementable methods where the user writes the instrumentation logic. BISM provides a fixed set of selectors classified into four categories: instruction, basic block, method, and meta selectors. We list below the set of available selectors and specify the execution they capture. A transformer can implement one or many selectors by overriding one of the below methods. Each overridden selector can invoke methods of the ```Advice Methods``` to append the source code . We list the available selectors and specify where the instrumented code will be executed:


* ```Before Instruction```
executes before a bytecode instruction. If the instruction is the entry point of a basic block, the code executes after the instruction.

* ```After Instruction```
executes after a bytecode instruction. If the instruction is the exit point of a basic block, the code executes before the instruction.

* ```Before Method Call```
executes before a method call, after loading any needed values on the stack.

* ```After Method Call```
executes immediately after a method call, before storing or popping the return value from the stack if any.
* ```On Basic Block Enter```
executes after the entry Label of the basic block.
* ```On Basic Block Exit```
executes after the last instruction of a basic block; except when last instruction is a JUMP/RETURN/THROW instruction, then it executes before the last instruction.
* ```On True Branch```
executes on the entry of the True successor block (applies only to conditional jumps).
* ```On False Branch```
executes on the entry of the False successor block (applies only to conditional jumps).
* ```On Method Enter```
executes on method entry block, rules of Before Basic Block apply here.
* ```On Method Exit```
executes on all exit blocks before the return or throw instruction.
* ```On Class Enter```
allows a developer to insert class fields


```java
void beforeInstruction(Instruction ins, InstructionDynamicContext dc);
void afterInstruction(Instruction ins, InstructionDynamicContext dc);
void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc);
void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc);
void onBasicBlockEnter(BasicBlock bb, InstructionDynamicContext dc);
void onBasicBlockExit(BasicBlock bb, InstructionDynamicContext dc);
void onTrueBranchEnter(BasicBlock bb, InstructionDynamicContext dc);
void onFalseBranchEnter(BasicBlock bb, InstructionDynamicContext dc);
void onMethodEnter(Method m, MethodDynamicContext dc);
void onMethodExit(Method m, MethodDynamicContext dc);
void onClassEnter(ClassContext c, ClassDynamicContext dc);

```
----
## Static Context

Static-context objects provide access to relevant static in- formation for captured joinpoints in selectors. Each selector has a specific static context object based on its category. These objects can be used to retrieve information about byte-code instructions, method calls, basic blocks, methods, and classes.

**```Instruction```** provides all relevant information about one instruction, and it contains the following fields:
* **index**:  a unique instruction index
* **node**:  the ASM ```org.objectweb.asm.tree.AbstractInsNode``` that can be casted into a more specific ASM AbstractInsNode sub type
* **opcode**: the bytecode instruction opcode
* **next**: the next instruction in the basic block, null if at the end of a block
* **previous**: the previous instruction in the basic block, null if at the beginning of a basic block
* **isConditionalJump()**: true if instruction is a conditional jump instruction
* **isBranchingInstruction()**: true if instruction is instanceof JumpInsnNode, LookupSwitchInsnNode, TableSwitchInsnNode; or opcode is ATHROW, RET, IRETURN, RETURN)
* **stackOperandsCountIfConditionalJump()**: the number of stack operands a conditional jump requires, else -1 for non conditional jumps
* **getBasicValueFrame()**: contains a list of all local variables and stack items and their types
* **getSourceValueFrame()**  contains a list of all local variables and stack items and their source i.e. what instruction last manipulated them.
* **basicBlock**: the ```BasicBlock``` context of this instruction
* **methodName**: the method, owner of this instruction
* **className**: the name of the class, owner of this instruction

**```MethodCall```** a special type of instruction that is available only before and after method calls and holds, in addition to the Instruction context, additional fields:
* **methodOwner**: the name of the method owner class
* **methodName**: the name of the method  called
* **currentClassName**: the name of the calling  class
* **methodnode**: the ASM ```MethodInsnNode``` instruction 
* **ins**: references the ```Instruction``` static context object 

**```BasicBlock```** the basic block context provides information about the current basic block, a basic block has a set of instructions it contains the following fields:
* **id**: a unique String id for the block
* **index**: a unique index id for the block in a class
* **blockType**: the block type: Normal, ConditionalJump, Goto, Switch, Return
* **size**: the number of instructions in the block
* **getSuccessorBlocks()**: all connected successor blocks of this block as per the cfg
* **getPredecessorBlocks()**: all connected predecessor blocks of this block as per the cfg
* **getTrueBranch()**: the target block after a conditional jump evaluates to true, is null if the block does not end with  a conditional jump
* **getFalseBranch()**: the target block after a conditional jump evaluates to false, is null if the block does not end with  a conditional jump
* **getFirstInstruction()**: the first ```AbstractInsNode``` in this basic block
* **getLastRealInstruction()**: the last real ```Instruction``` (opcode != -1) in this basic block
* **getFirstRealInstruction()**: the first real ```Instruction``` (opcode != -1) in this basic block
* **method**: the ```Method``` context of this basic block


**```Method```** the method context provides info about the method being instrumented and has the following fields:
* **name**: the name of the method
* **className**: the name of the class owner of the method
* **methodNode**: the ASM org.objectweb.asm.tree.MethodNode
* **getNumberOfBasicBlocks()**: the number of basic blocks in the method
* **getEntryBlock()**: the entry basic block ```BasicBlock```
* **getExitBlocks()**: a list of all exiting basic blocks ```BasicBlock```
* **classContext**: Class context of this method

**```ClassContext```** the class context provides info about the class being instrumented and has the following fields:
* **name**: the name of the method
* **classNode**: the ASM org.objectweb.asm.tree.ClassNode
----

## Dynamic Context

BISM also provides dynamic context objects at selectors to extract joinpoint dynamic information. These objects can access dynamic values from captured joinpoints that are possibly only known during the base program execution. BISM gathers this information from local variables and operand stack, then weaves the necessary code to extract this information.

They are methods that return a special type ```DynamicValue``` which is a reference to a local variable. These local variable are either already created by the compiler or created specially by bism to hold the context requested. Some context is not available at certain join points. For example ```getMethodArgs()``` is only available at ```MethodCall``` joint points. We list the objects that implement these methods, then we show the ```DynamicValue``` type, and then describe these methods:



```InstructionDynamicContext```
available at Instruction and BasicBlock join points.

```MethodCallDynamicContext```
available at MethodCall join points.

```MethodDynamicContext```
available at Method join points.


 ```DynamicValue``` this class is generated automatically by calling dynamic context methods.
* **index**: integer index of the local variable that holds the value
* **type**: org.objectweb.asm.Type denotes the type of value
* **descriptor**: String descriptor of the type

### Common Dynamic Context
All of the below methods take in an extra argument which is the StaticContext object mentioned above at each join point (omitted for brevity) and return a ```DynamicValue``` type.

```DynamicValue getThis()```
* Returns a reference to ```this``` of the class being instrumented, and null if class is static.
* Available in all join points .

```DynamicValue getThreadName()```
* Gets the thread name executing a method

```DynamicValue getStackValue(int i)```
* Returns a reference to a stack value
* Available in all join points.

```DynamicValue getLocalVariable(int i)```
* Return a reference to a local variable by index.
* Available at all join points.

```LocalVariable updateLocalVariable(LocalVariable dv, Object primitiveValue)```
* Updates an LocalVariable and sets it to a primitive value
* Available in all join points.

 

#### Class Fields

```DynamicValue getInstanceField(String fieldName)```
* Gets a fields by name from the calling class

```DynamicValue getInstanceField(DynamicValue dv, String fieldName, Class c)```
* Gets a field from an instance of a class held in a dynamic value
* Must specify Class/type

```setInstanceField(String fieldName, DynamicValue dv)```
* Sets a field from a dynamic value

```setInstanceField(String fieldName, Object pv)``` 
* Sets a field from a static value

```DynamicValue getStaticField(String fieldName)```
* Gets a fields by name from the calling class

```DynamicValue getStaticField(DynamicValue dv, String fieldName, Class c)```
* Gets a field from an instance of a class held in a dynamic value
* Must specify Class/type

```setStaticField(String fieldName, DynamicValue dv)```
* Sets a field from a dynamic value

```setStaticField(String fieldName, Object pv)``` 
* Sets a field from a static value


#### Local Arrays
```clearLocalArray(LocalArray la)```
* Clears the content of a synthetic local array

```DynamicValue getLocalArrayValue(LocalArray la, int index)```
* Returns a dynamic value containing the value la[index]

```setLocalArrayValue(LocalArray la, int index, Object value)```
* Set a static value into a local array ( la[index] = value )

```setLocalArrayValue(LocalArray la, int index, DynamicValue value)```
* Set a dynamic value into a local array ( la[index] = value )

```addToLocalArray(LocalArray la, Object value)```
* Appends a value to a local array (considered as a List)

```addToLocalArray(LocalArray la, DynamicValue value)```
* Appends a dynamic value to a local array (considered as a List)

```DynamicValue getLocalArraySize(LocalArray la)```
* Gets the size of a local array


### Method Joinpoint specific 

```LocalArray createLocalArray(Class listType)```
* Creates a synthetic Array List in a method
* Only supports primitve types + String

```LocalVariable addLocalVariable(Object primitiveValue)```
* Creates a synthetic local variable in a method
* Only supports primitve types + String


### MethodCall Joinpoint specific 
```DynamicValue getMethodReceiver()```
* Return a reference to the object whose method is being called. Returns null for static methods.
* Available only in method call join points

```DynamicValue getMethodArgs(int i)```
* Returns a reference to a method argument by index. Starts at 1
* Available in method and method call join points.

```DynamicValue getMethodResult()```
* Returns a reference to a method result
* Available in method call join points only.
 



# Examples and Experiments

The repository for examples can be found at [BISM-examples](https://gitlab.inria.fr/bism/bism-examples).

The repository for experiments can be found at [BISM-experiments](https://gitlab.inria.fr/bism/bism-experiments)


 # Contact Us

Chukri Soueidi chukri.a.soueidi@inria.fr
Yliès Falcone ylies.falcone@univ-grenoble-alpes.fr

 



    

